# Runners Gitlab

## Sumário 
 - [ Objetivo ](#Objetivo) 
 - [ Abrangência ](#Abrangencia) 
 - [ Conceitos e Definições ](#Conceitos) 
 - [ Fluxo de atividades ](#Atividades) 
 - [ Referências ](#Referencias) 

#### Objetivo: <a name='Objetivo'></a> 
Descrever como instalar o agente do GitLab Runner para Shell e Docker em um servidor específico.

#### Abrangência: <a name='Abrangencia'></a> 
 Descrever a finalidade do procedimento, considerando também sua relevância e justificativa. 
Somente servidores Linux se encaixam neste procedimento operacional. O processo de instalação e configuração em Sistemas Windows não é abrangente.

O GitLab Runner é o projeto de código aberto usado para executar seus trabalhos e enviar os resultados de volta ao GitLab. É usado em conjunto com o [GitLab CI ](https://about.gitlab.com/product/continuous-integration/), o serviço de integração contínua de código aberto incluído no GitLab que coordena os trabalhos.

Permite executar:
- Vários trabalhos simultaneamente.
- Use vários tokens com vários servidores (mesmo por projeto).
- Limite o número de trabalhos simultâneos por token.
- Os trabalhos podem ser executados:
  - Localmente.
  - Usando contêineres do Docker.
  - Usando contêineres do Docker e executando tarefas no SSH.
  - Usando contêineres do Docker com dimensionamento automático em diferentes nuvens e hipervisores de virtualização.
  - Conectando ao servidor SSH remoto.
- É escrito em Go e distribuído como binário único, sem quaisquer outros requisitos.
- Oferece suporte ao Bash, Windows Batch e Windows PowerShell.
- Funciona no GNU / Linux, macOS e Windows (praticamente em qualquer lugar que você possa executar o Docker).
- Permite a personalização do ambiente de execução da tarefa.
- Recarregamento automático de configuração sem reinicialização.
- Configuração fácil de usar, com suporte para ambientes em execução Docker, Docker-SSH, Parallels ou SSH.
- Permite o armazenamento em cache de contêineres do Docker.
- Fácil instalação como um serviço para GNU / Linux, macOS e Windows.
- Servidor HTTP de métricas do Prometheus incorporado.

#### Conceitos e Definicões: <a name='Conceitos'></a> 

A versão do GitLab Runner deve estar sincronizada com a versão do GitLab. Enquanto os Runners mais antigos ainda podem trabalhar com versões mais recentes do GitLab, e vice-versa, em alguns casos, os recursos podem não estar disponíveis ou funcionar corretamente se houver uma diferença de versão.


#### Fluxo de atividades: <a name='Atividades'></a> 
 
1. Instale o GitLab Runner Docker

    1.1 Deve ser criado um container para utiliza-lo como runner de imagens Docker:

  ```
 docker run -dit \
--name runner-docker \
--restart always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /opt/gitlab-runner/config:/etc/gitlab-runner \
gitlab/gitlab-runner:ubuntu-v12.1.0-rc1
  ```

1.2. Após a criação do container, deve ser coletado as informações do Token do Gitlab. Faça login no seu Gitlab -> Área de administrador -> Virão geral -> Runners:
![](runner.png)

1.3 Deve ser executado o comando para habilitar este container como runner do GitLab:

```
docker exec -it runner-docker \
gitlab-runner register -n \
--url http://172.20.0.85 \
--registration-token hn95zry4N5tHesWiiz9N \
--clone-url http://172.20.0.85 \
--executor docker \
--docker-image "docker:latest" \
--docker-privileged
```

1.4 O Runner já foi habilitado para executar tarefas com imagens de container, após voltar ao GitLab aparecerá o node adicionado.

![](runner-2.png)

As configurações deste node deve ficar da seguinte maneira:

**Ativo** = Runners pausados não aceitam novas tarefas


**Rodar tarefas sem tags** = Indica se este runner pode escolher tarefas sem tags

2. instalar GitLab Runner Shell diretamente no servidor para executar comandos shell.
    2.1 Instale o GitLab Runner.
    ```
    apt-get install gitlab-runner
    ```
    
    2.2 Adicionar gitlab-runnerusuário ao dockergrupo:
    ```
    usermod -aG docker gitlab-runner
    ```
    
2.3 Após a instalação do GitLab Runner, selecione shell como método de execução de scripts de tarefas utilizando o comando:

```
gitlab-runner register -n \
  --url https://versionamento.anvisa.gov.br/ \
  --registration-token TOKEN-AQUI \
  --executor shell \
  --description "Runner Shell NOME-DO-SERVER"
```
   2.4 Verifique se gitlab-runner tem acesso ao Docker:
   ```
   sudo -u gitlab-runner -H docker info
   ```
   Após voltar ao GitLab aparecerá o node adicionado.
   
   ![](runner-2.png)
   

#### Referências: <a name='Referencias'></a> 
 - [https://medium.com/@rukeith/how-to-use-docker-to-build-a-self-host-gitlab-and-gitlab-runner-781981dc4d03](https://medium.com/@rukeith/how-to-use-docker-to-build-a-self-host-gitlab-and-gitlab-runner-781981dc4d03)
 - [https://docs.gitlab.com/runner/install/docker.html](https://docs.gitlab.com/runner/install/docker.html)
 - [https://docs.gitlab.com/runner/](https://docs.gitlab.com/runner/)

|  Autor |  Claudio Antonio da Silva  | 
 |  :-----  |  :-----  | 
 |  Contato |  claudiosilva@duzeru.org | 
